Suported OS
* Cent OS 7
* macOS 10.15.3

Required packages
*  PyQt5
*  OpenCV
*  dbinterface [link](https://gitlab.cern.ch/sshirabe/dbinterface)
*  local DB [link](https://localdb-docs.readthedocs.io/en/master/)

install PyQt5

    pip3 install PyQt5

install OpenCV

    pip3 install opencv-python

    pip3 install opencv-python==4.1.2.30 (for macOS)
    
install dbinterface

    git clone https://gitlab.cern.ch/sshirabe/dbinterface.git
    
**Quick Tutorial**

Setup

    mkdir Workdir
    
    cd Workdir
    
    git clone https://gitlab.cern.ch/sshirabe/dbinterface.git

    cd dbinterface

    git pull devel_onishi
    
    cd ..

    git clone "~~~~~~~~~~~~~~~~"
    
    mkdir each_non_electrical_test
    
    cd each_non_electrical_test
    
    git clone [URL of each non electrical test tool]
    
Run VI GUI

    cd GUI
    
    python3 bin/main.py
    

Environmental Debug

    If you get the error [qt.qpa.plugin: Could not load the Qt platform plugin "xcb" in "" even though it was found.] in ececuting 
    $python3 bin/main.py,
    
    you should do below at first.
    $export QT_DEBUG_PLUGINS=1

    Then, you can get yhe message which show what is wrong.
    
    If you get below message, you have to get shared library "libxcb—icccm.so.4".
    
    Cannot load library /usr/local/lib64/python3.6/site-packages/PyQt5/Qt/plugins/platforms/libqxcb.so: (libxcb—icccm.so.4: cannot open shared object file: No such file or directory)
    
    You can search what package you should download to get above shared library by using https://pkgs.org.

    If you can find out what package you need, you should download it
    #yum install xcb-util-wm.x86_64        (for libxcb—icccm.so.4 in CentOS7)

    After finifh downloading, you can check if you succeeded to download the shared library.
    #find /* -name libxcb-icccm.so.4 