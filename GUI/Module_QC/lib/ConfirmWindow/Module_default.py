import os
import sys
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sip
import json
from bson.objectid import ObjectId
import pprint
#import datetime
from datetime import date, datetime
import traceback

sys.path.append(os.path.join(os.path.dirname(__file__), '../../../../dbinterface/lib'))
import db_retriever

class ConfirmWindow(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__(parent)
        self.parent = parent

        titlebox= QVBoxLayout()
        layout = QVBoxLayout()
        button_box = QHBoxLayout()

        label_title = QLabel()
        label_title.setText('<center><font size="7">Please Confirm before upload to database</font></center>')
        label_practice = QLabel()
        label_practice.setText('<center><font size="4" color = "green"> Practice Mode</font></center>')
        Upload_button= QPushButton("&Upload!")
        Upload_button.clicked.connect(self.upload_to_db)
        json_button= QPushButton("&Check json (for expert)")
        json_button.clicked.connect(self.check_json)
        back_button = QPushButton('&Back')
        back_button.clicked.connect(self.back_page)
        
        titlebox.addWidget(label_title)
        if self.parent.isPractice:
            layout.addWidget(label_practice)

        button_box.addWidget(back_button)
        button_box.addStretch()
        button_box.addWidget(json_button)
        button_box.addWidget(Upload_button)

        inner = QScrollArea()
        result_wid = QWidget()
        result_wid.setLayout( self.make_layout(self.parent.result_dict) )
        inner.setWidgetResizable(True)
        inner.setWidget(result_wid)

        layout.addLayout(titlebox)
        layout.addWidget(inner)
        layout.addLayout(button_box)
        self.setLayout(layout)

    def back_page(self):
        self.parent.back_to_test()

    def check_json(self):
        self.parent.confirm_json()

    def upload_to_db(self):
        self.parent.upload_to_db()

    def add_layout_for_moduleQC(self,Form_layout,label_str,form_text):
        
        label = QLabel()
        label.setText(label_str)

        editor = QPlainTextEdit()
        editor.setPlainText(form_text)
        editor.setReadOnly(True)
        editor.setStyleSheet("background-color : linen")
        #        editor.setStyleSheet("background-color : azure")
        Form_layout.addRow(label,editor)
        
    def make_layout(self,result_dict):
        
        Form_layout = QFormLayout()
        
        self.add_layout_for_moduleQC(Form_layout,'your name',result_dict['user'])
        self.add_layout_for_moduleQC(Form_layout,'your institution',result_dict['institution'])
        self.add_layout_for_moduleQC(Form_layout,'test stage',result_dict['localDB']['stage'])
        self.add_layout_for_moduleQC(Form_layout,'test name',result_dict['testType'])
        self.add_layout_for_moduleQC(Form_layout,'time',str(result_dict['localDB']['sys']['cts']))

        for result_key in result_dict['results']:
            self.add_layout_for_moduleQC(Form_layout,result_key,result_dict['results'][result_key])
        
        return Form_layout
