import os
import sys
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sip
import json
from bson.objectid import ObjectId
import pprint
#import datetime
from datetime import date, datetime
import traceback

sys.path.append(os.path.join(os.path.dirname(__file__), '../../../../dbinterface/lib'))
import db_retriever

class ConfirmWindow(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__(parent)
        self.parent = parent

        titlebox= QVBoxLayout()
        layout = QVBoxLayout()
        button_box = QHBoxLayout()

        label_title = QLabel()
        label_title.setText('<center><font size="7">Please Confirm before upload to database</font></center>')
        label_practice = QLabel()
        label_practice.setText('<center><font size="4" color = "green"> Practice Mode</font></center>')
        Upload_button= QPushButton("&Upload!")
        Upload_button.clicked.connect(self.upload_to_db)
        json_button= QPushButton("&Check json (for expert)")
        json_button.clicked.connect(self.check_json)
        back_button = QPushButton('&Back')
        back_button.clicked.connect(self.back_page)
        
        titlebox.addWidget(label_title)
        if self.parent.isPractice:
            layout.addWidget(label_practice)

        button_box.addWidget(back_button)
        button_box.addStretch()
        button_box.addWidget(json_button)
        button_box.addWidget(Upload_button)

        inner = QScrollArea()
        result_wid = QWidget()
        result_wid.setLayout( self.make_layout(self.parent.result_dict) )
        inner.setWidgetResizable(True)
        inner.setWidget(result_wid)

        layout.addLayout(titlebox)
        layout.addWidget(inner)
        layout.addLayout(button_box)
        self.setLayout(layout)

    def back_page(self):
        self.parent.back_to_test()

    def check_json(self):
        self.parent.confirm_json()

    def upload_to_db(self):
        self.parent.upload_to_db()

    def add_layout(self,Form_layout,label_str,form_text):
        
        label = QLabel()
        label.setText(label_str)

        editor = QPlainTextEdit()
        editor.setPlainText(form_text)
        editor.setReadOnly(True)
        editor.setStyleSheet("background-color : linen")
        #        editor.setStyleSheet("background-color : azure")
        Form_layout.addRow(label,editor)

    def nest_result_layout(self,Form_layout,key_str,nest_dict,loop):
        if loop > 0:
            for key in nest_dict:
                if isinstance(nest_dict[key], dict):
                    self.nest_result_layout(Form_layout,key_str + key + ':',nest_dict[key],loop-1)
                else:
                    self.add_layout(Form_layout, key_str + key + ':' ,json.dumps(nest_dict[key], default=self.json_rule, indent=4))

    def make_layout(self,result_dict):

        Form_layout = QFormLayout()
        self.nest_result_layout(Form_layout,'',result_dict,5)

        return Form_layout

    def json_rule(self,obj):
        if isinstance(obj, (datetime, date)):
            return obj.isoformat()
        if isinstance(obj, ObjectId):
            return str(obj)
            raise TypeError ("Type %s not serializable" % type(obj))
