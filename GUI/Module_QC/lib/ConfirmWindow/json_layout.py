import os
import sys
import traceback
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sip
import json
from bson.objectid import ObjectId
import pprint
#import datetime
from datetime import date, datetime



# from PyQt5.QtWidgets import *
# from PyQt5.QtGUI import *
# from PyQt5.QtCore import *

class JsonWindow(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__(parent)
        self.parent = parent

        titlebox= QVBoxLayout()
        vbox_whole = QVBoxLayout()
        hbox_text = QHBoxLayout()
        hbox_bottom = QHBoxLayout()

        
        Upload_button= QPushButton("&Upload!")
        Upload_button.clicked.connect(self.upload_to_db)
        Back_button= QPushButton("&Back")
        Back_button.clicked.connect(self.back_to_confirm)
        Write_button= QPushButton("&Write to json file")
        Write_button.clicked.connect(self.write_to_json)

        label_title = QLabel()
        label_title.setText('<center><font size="7">Please Confirm before upload to database</font></center>')
        label_text = QLabel()
        label_text.setText('<center><font size="5">Result is displayed as json format</font></center>')

        upload_text = QTextEdit()
        upload_text.setText( json.dumps(self.parent.result_dict,default=self.json_rule,indent=4) )
        upload_text.setReadOnly(True)
        
        inner = QScrollArea()
        inner.setWidgetResizable(True)
        inner.setWidget(upload_text)
        
        titlebox.addWidget(label_title)
        titlebox.addWidget(label_text)

        hbox_text.addWidget(inner)

        hbox_bottom.addWidget(Back_button)
        hbox_bottom.addStretch()
        hbox_bottom.addWidget(Write_button)
        hbox_bottom.addWidget(Upload_button)

        vbox_whole.addLayout(titlebox)
        vbox_whole.addLayout(hbox_text)
        vbox_whole.addLayout(hbox_bottom)
        self.setLayout(vbox_whole)

    def back_to_confirm(self):
#        self.parent.back_to_result()
        self.parent.comfirm_result()

    def write_to_json(self):

        if self.parent.result_dict['component'] !='' and self.parent.result_dict['currentStage'] !='' and self.parent.result_dict['testType'] !='':
            file_name = self.parent.result_dict['component'] + '_' + self.parent.result_dict['currentStage'] + '_' + self.parent.result_dict['testType'] +'.json'
        else:
            file_name = datetime.now().strftime('%YY%mm%dd__%H_%M_%S') +'.json'

        file_path=os.path.join(os.path.dirname(__file__), '../../json/'+file_name)

        try:
            with open(file_path, 'w') as f:
                json.dump(self.parent.result_dict, f, default=self.json_rule, indent=4)
            QMessageBox.information(None, 'Write result','to ' + file_path, QMessageBox.Ok)
        except:
            print (traceback.format_exc() )
            QMessageBox.warning(None, 'Worning','failed to write result to ' + file_path, QMessageBox.Ok)
            
    def upload_to_db(self):
        self.parent.upload_to_db()

    def json_rule(self,obj):
        if isinstance(obj, (datetime, date)):
            return obj.isoformat()
        if isinstance(obj, ObjectId):
            return str(obj)
        raise TypeError ("Type %s not serializable" % type(obj))



