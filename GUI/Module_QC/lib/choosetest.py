import sys
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sip

# from PyQt5.QtWidgets import *
# from PyQt5.QtGUI import *
# from PyQt5.QtCore import *

class InitialWindow(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__(parent)
        self.parent = parent

        titlebox= QVBoxLayout()
        vbox = QVBoxLayout()
        hbox = QHBoxLayout()
#        layout = QGridLayout()
        layout = QFormLayout()
        
        label_text = QLabel()
        label_text.setText('<center><font size="7">Welcome to localDB uploader!!</font></center>')
        label_inspector = QLabel()
        label_inspector.setText('Inspector:')
        label_moduleid = QLabel()
        label_moduleid.setText('Module ID:')
        label_stage = QLabel()
        label_stage.setText('Stage:')
        label_institute = QLabel()
        label_institute.setText('Institute:')
        label_whichtest = QLabel()
        label_whichtest.setText('Test type:')

        self.edit_inspector = QLineEdit()
        self.edit_inspector.setText(self.parent.inspector)
        self.edit_inspector.setMaxLength(50)

        self.edit_moduleid = QLineEdit()
        self.edit_moduleid.setText(self.parent.moduleid)
        self.edit_moduleid.setMaxLength(50)

        self.edit_stage = QLineEdit()
        self.edit_stage.setText(self.parent.stage)
        self.edit_stage.setMaxLength(50)

        self.edit_institute = QLineEdit()
        self.edit_institute.setText(self.parent.institute)
        self.edit_institute.setMaxLength(50)

        button= QPushButton("&Continue")
        button.clicked.connect(self.pass_moduleid)
        self.choose_test=QComboBox()
        self.choose_test.addItem('------ Select test type -----')        
        self.choose_test.addItems(self.parent.test_list)
        
        titlebox.addWidget(label_text)

        ### input Form ###
        layout.addRow(label_inspector,self.edit_inspector)
        layout.addRow(label_institute,self.edit_institute)
        layout.addRow(label_moduleid,self.edit_moduleid)
        layout.addRow(label_stage,self.edit_stage)
        layout.addRow(label_whichtest,self.choose_test)
        ##################

        hbox.addStretch()
        hbox.addWidget(button)

        vbox.addStretch()
        vbox.addLayout(titlebox)
        vbox.addStretch()
        vbox.addLayout(layout)
        vbox.addLayout(hbox)
        self.setLayout(vbox)


    def pass_moduleid(self):
        if self.choose_test.currentText()=="------ Select test type -----":
            QMessageBox.warning(None, 'Worning','Please Fill all box', QMessageBox.Ok)
            self.parent.init_ui()
        elif self.edit_moduleid.text()=="": 
            QMessageBox.warning(None, 'Worning','Please Fill all box', QMessageBox.Ok)
            self.parent.init_ui()
        elif self.edit_stage.text()=="":
            QMessageBox.warning(None, 'Worning','Please Fill all box', QMessageBox.Ok)
            self.parent.init_ui()
        elif self.edit_inspector.text()=="":
            QMessageBox.warning(None, 'Worning','Please Fill all box', QMessageBox.Ok)
            self.parent.init_ui()
        elif self.edit_institute.text()=="":
            QMessageBox.warning(None, 'Worning','Please Fill all box', QMessageBox.Ok)
            self.parent.init_ui()


        else:
            self.parent.recieve_moduleid(self.edit_moduleid.text(),self.edit_stage.text(),self.edit_inspector.text(),self.edit_institute.text(),self.choose_test.currentText())
            #self.parent.recieve_moduleid(self.edit_moduleid.text(),self.edit_stage.text(),self.edit_inspector.text(),self.edit_institute.text(),self.test_type)
        
