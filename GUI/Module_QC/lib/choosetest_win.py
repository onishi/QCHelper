import sys, os
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sip
import pymongo
from bson.objectid import ObjectId
import traceback


sys.path.append(os.path.join(os.path.dirname(__file__), '../../dbinterface/lib'))
#import localdb_retriever
import db_retriever

class ChooseTestWindow(QWidget):
    def __init__(self, parent=None):
#        super(QWidget, self).__init__(parent)
        super(QWidget, self).__init__()
        self.parent = parent

        next_button = QPushButton('&Next')
        next_button.clicked.connect(self.next_page)
        back_button = QPushButton('&Back')
        back_button.clicked.connect(self.back_page)

        button_box = QHBoxLayout()
        button_box.addWidget(back_button)
        button_box.addStretch()
        
        self.test_dict = self.Fill_test_dict(self.parent.result_dict['currentStage'])
        
        label_title = QLabel()
        label_title.setText('<center><font size="7"> Which test do you want to do?</font></center>')
        label_practice = QLabel()
        label_practice.setText('<center><font size="4" color = "green"> Practice Mode</font></center>')
        label_uploaded = QLabel()
        label_uploaded.setText('<center><font size="2"> already uploaded</font></center>')
        
        layout = QVBoxLayout()            
        radio_box = QGridLayout()
        radio_Hbox = QHBoxLayout()            
        info_layout = self.make_layout(self.parent.result_dict)
        
        self.radiobutton_list = []
#        self.testlabel_list = []         #######now debugging
        self.radio_group = QButtonGroup()
        check = []

        radio_box.addWidget(label_uploaded, 0,1)
        for i,test_title in enumerate( self.test_dict.keys() ):
            self.radiobutton_list.append(self.make_radiobutton(test_title))
            self.radio_group.addButton(self.radiobutton_list[i],i)
            #            isUploaded = self.check_uploaded( self.test_dict[test_title] )         #######now debugging
            isUploaded = False
            
            check.append( QCheckBox() )
            check[i].setAttribute(Qt.WA_TransparentForMouseEvents);
            check[i].setFocusPolicy(Qt.NoFocus);
#            if isUploaded:         #######now debugging
#                check[i].setCheckState(Qt.Checked)         #######now debugging
                            
            radio_box.addWidget(self.radiobutton_list[i], i+1,0)
            radio_box.addWidget(check[i], i+1,1)
#            self.testlabel_list.append( self.check_uploaded( self.test_dict[test_title] ) )         #######now debugging
#            radio_box.addRow(self.testlabel_list[i],self.radiobutton_list[i])         #######now debugging
            
        
        button_box.addWidget(back_button)
        button_box.addStretch()
        button_box.addWidget(next_button)
        
        radio_Hbox.addStretch()
        radio_Hbox.addLayout(radio_box)
        radio_Hbox.addStretch()
        
        
        layout.addWidget(label_title)
        if self.parent.isPractice:
            layout.addWidget(label_practice)
        layout.addStretch()
        layout.addLayout(info_layout)
        layout.addStretch()
        layout.addLayout(radio_Hbox)
        layout.addStretch()
        layout.addLayout(button_box)
        self.setLayout(layout)
                
    def next_page(self):
        try:
            self.choose_target()
            self.parent.call_targetGUI()
        except:
            QMessageBox.warning(None, 'Worning','Please choose test you want to do', QMessageBox.Ok)

                
    def back_page(self):
        self.parent.typebranch()

    
    def check_uploaded(self,test_to_check):

        try:
            db_retriever.localdb_auth_retriever(self.parent.db_user,self.parent.db_pass,self.parent.DBname,self.parent.col_dict['testResult'],{'component':self.parent.result_dict['localDB']['component'],'stage':self.parent.result_dict['localDB']['stage'],'testType':test_to_check  },'_id')
            isUploaded = True
        except TypeError:
            import traceback
            traceback.print_exc() 
#            print( traceback.print_exc() )
            isUploaded = False
        return isUploaded

    def make_radiobutton(self,label):
        radiobutton = QRadioButton(label)
        radiobutton.setCheckable(True)
        radiobutton.setFocusPolicy(Qt.NoFocus)
#        radiobutton.setStyleSheet('QRadioButton{font: 20pt;} QRadioButton::indicator { width: 25px; height: 25px;};')
        
        if label == self.parent.result_dict['testType']:
            radiobutton.setChecked(True)
        return radiobutton


    def choose_target(self):
        self.parent.result_dict['testType'] = self.test_dict[ self.radio_group.checkedButton().text() ]
        self.parent.result_dict['localDB']['testType'] = self.test_dict[ self.radio_group.checkedButton().text() ]
            

    def make_layout(self,result_dict):

        layout = QHBoxLayout()
        vlayout = QVBoxLayout()

        self.add_infotable(vlayout,'Serial Number',self.parent.result_dict['component'])
#        self.add_infotable(vlayout,'test stage',self.parent.result_dict['localDB']['stage'])        ####?? which one is good???
        self.add_infotable(vlayout,'test stage',self.parent.result_dict['currentStage'])             ####??
        self.add_infotable(vlayout,'your name',self.parent.result_dict['user'])
        self.add_infotable(vlayout,'Institution',self.parent.result_dict['institution'])

        layout.addStretch()
        layout.addLayout(vlayout)
        layout.addStretch()
        return layout


    def add_infotable(self,vlayout,label_str,info_str):

        hbox_label = QHBoxLayout()
        hbox_info = QHBoxLayout()

        label_text = QLabel()
        label_text.setText('<center><font size="4">'+label_str +'</font></center>')
        info_text = QLabel()
        info_text.setText('<center><font size="5">'+info_str +'</font></center>')

        hbox_label.addWidget(label_text)
        hbox_label.addStretch()

        hbox_info.addStretch()
        hbox_info.addWidget(info_text)
        hbox_info.addStretch()

        vlayout.addLayout(hbox_label)
        vlayout.addLayout(hbox_info)

    def Fill_test_dict(self,current_stage):
        
        test_dict = {
            'MODULEBARE':{
                'Optical_Inspection':'OPTICAL',
                'Mass_Measurement':'MASS',
                'Bare_Module_Metrology':'METROLOGY_BARE'
            },
            'MODULETOPCB':{
                'Optical_Inspection':'OPTICAL',
                'Mass_Measurement':'MASS',
                'Module+Flex_Attach_Metrology':'METROLOGY_FLEX_ATTACH',
                'Glue_Information_Module+Flex_Attach':'GLUE_MODULE_FLEX_ATTACH'
            },
            'MODULEWIREBONDING':{
                'Optical_Inspection':'OPTICAL',
                'Wirebonding_Information':'WIREBONDING',
                'Wirebond_pull_tests':'WIREBOND',
                'IV_Curve':'IV_CURVE',
            },
            'MODULEWIREBONDPROTECTION':{
                'Optical_Inspection':'OPTICAL',
                'Mass_Measurement':'MASS',
                'Glue_Information_Potting':'POTTING',
                'IV Curve':'IV_CURVE',
                'Basic Electrical Tests':'BASIC_ELECTRICAL'
            },
            'MODULEPARYLENECOATING':{
                'Optical_Inspection':'OPTICAL',
                'Mass_Measurement':'MASS',
                'Parylene Properties':'PARYLENE',
                'IV Curve':'IV_CURVE',
                'Basic Electrical Tests':'BASIC_ELECTRICAL'
            },
            'MODULETHERMALCYCLING':{
                'Optical_Inspection':'OPTICAL',
                'IV Curve':'IV_CURVE',
                'Basic Electrical Tests':'BASIC_ELECTRICAL',
                'Thermal Cycling':'THERMAL_CYCLING'
            },
            'MODULEBURNIN':{
                'Optical_Inspection':'OPTICAL',
                'Mass_Measurement':'MASS',
                'Final Module Metrology':'METROLOGY_FINAL',
                'IV_Curve':'IV_CURVE',
            },
            'localDB_practice':{
                'Optical_Inspection':'OPTICAL',
                'Mass_Measurement':'MASS',
            },
            'Burn-in':{
                'Optical_Inspection':'OPTICAL',
                'Mass_Measurement':'MASS',
                'IV Curve':'IV_CURVE',
                'Final Module Metrology':'METROLOGY_FINAL',
            },
            'plactice':{
                'Mass_Measurement':'MASS',
                'test1':'TEST1',
                'test2':'TEST2',
                'test3':'TEST3',
                'test4':'TEST4'
            }
        }
        test_dict_error = {
            'Not_applicable':'None'
        }

        try:
            return test_dict[current_stage]
        except Exception as e:
            return test_dict_error
            
