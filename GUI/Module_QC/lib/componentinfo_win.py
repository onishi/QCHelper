import sys
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sip

# from PyQt5.QtWidgets import *
# from PyQt5.QtGUI import *
# from PyQt5.QtCore import *

class ComponentInfoWindow(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__(parent)
        self.parent = parent


        layout = QVBoxLayout()

        label_text = QLabel()
        label_text.setText('<center><font size="7">Component Information</font></center>')

        Next_button= QPushButton("&Next")
        Next_button.clicked.connect(self.go_to_qc)        
        Back_button= QPushButton("&Back")
        Back_button.clicked.connect(self.back_page)
        

        layout.addWidget(label_text)
        self.add_infotable(layout,'ATLAS Serial Number:',self.parent.result_dict['component'])
        self.add_infotable(layout,'Component Type',self.parent.result_dict['componentType'])
        self.add_infotable(layout,'Institution',self.parent.result_dict['institution'])
        self.add_infotable(layout,'Current Location',self.parent.result_dict['currentLocation'])
        self.add_infotable(layout,'Component Name',self.parent.result_dict['type'])
        self.add_infotable(layout,'Current Stage:',self.parent.result_dict['currentStage'])
        self.add_button(layout,Back_button,Next_button)

        self.setLayout(layout)

    def add_button(self,vlayout,back_button,next_button):
        hbox = QHBoxLayout()
        
        hbox.addWidget(back_button)
        hbox.addStretch()
        hbox.addWidget(next_button)

        vlayout.addLayout(hbox)

    def add_infotable(self,vlayout,label_str,info_str):
        
        hbox_label = QHBoxLayout()
        hbox_info = QHBoxLayout()

        label_text = QLabel()
#        label_text.setText(label_str)
        label_text.setText('<center><font size="4">'+label_str +'</font></center>')
        info_text = QLabel()
#        info_text.setText(info_str)
        info_text.setText('<center><font size="5">'+info_str +'</font></center>')

        hbox_label.addWidget(label_text)
        hbox_label.addStretch()

        hbox_info.addStretch()
        hbox_info.addWidget(info_text)
        hbox_info.addStretch()
        
        vlayout.addLayout(hbox_label)
        vlayout.addLayout(hbox_info)
        
    def go_to_qc(self):
        self.parent.typebranch()

    def back_page(self):
        self.parent.init_ui()
