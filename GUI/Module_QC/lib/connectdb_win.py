import sys
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sip

# from PyQt5.QtWidgets import *
# from PyQt5.QtGUI import *
# from PyQt5.QtCore import *

class ConnectDBWindow(QWidget):
    def __init__(self, parent=None):
#        super(QWidget, self).__init__(parent)
        super(QWidget, self).__init__()
        self.parent = parent

#        layout = QGridLayout()
        layout = QVBoxLayout()
        hbox_bottom = QHBoxLayout()
        edit_form = QFormLayout()
        form_box = QHBoxLayout()        

        label_text = QLabel()
        label_text.setText('<center><font size="7">Access to Local DB</font></center>')
        label_user = QLabel()
        label_user.setText('User:')
        label_password = QLabel()
        label_password.setText('Password:')        

        self.edit_user = QLineEdit()
        self.edit_user.setText(self.parent.db_user)
        self.edit_password = QLineEdit()        
        self.edit_password.setEchoMode(QLineEdit().Password)        
        
        Next_button= QPushButton("&Next")
        Next_button.clicked.connect(self.go_to_modqc)
        Back_button= QPushButton("&Back")
        Back_button.clicked.connect(self.back_page)
        

        hbox_bottom.addWidget(Back_button)
        hbox_bottom.addStretch()
        hbox_bottom.addWidget(Next_button)

        edit_form.addRow(label_user,self.edit_user)
        edit_form.addRow(label_password,self.edit_password)

        form_box.addSpacing(5)
        form_box.addLayout(edit_form)
        form_box.addSpacing(Next_button.sizeHint().width()+6)


        layout.addStretch()
        layout.addWidget(label_text)
        layout.addStretch()
        layout.addLayout(form_box)
        layout.addLayout(hbox_bottom)

        self.setLayout(layout)

    def go_to_modqc(self):
        self.parent.choose_test(self.edit_user.text(),self.edit_password.text())

    def back_page(self):
        self.parent.see_info()
