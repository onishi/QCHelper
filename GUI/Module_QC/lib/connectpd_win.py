import sys
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sip

# from PyQt5.QtWidgets import *
# from PyQt5.QtGUI import *
# from PyQt5.QtCore import *

class ConnectPDWindow(QWidget):
    def __init__(self, parent=None):
#        super(QWidget, self).__init__(parent)
        super(QWidget, self).__init__()
        self.parent = parent

        titlebox= QVBoxLayout()
        layout = QVBoxLayout()
        hbox_bottom = QHBoxLayout()
        edit_form = QFormLayout()
        form_box = QHBoxLayout()


        label_text = QLabel()
        label_text.setText('<center><font size="7">Access to Production DB</font></center>')
        label_accesscode1 = QLabel()
        label_accesscode1.setText('ITKDB ACCESS CODE 1:')
        label_accesscode2 = QLabel()
        label_accesscode2.setText('ITKDB ACCESS CODE 2:')        

        self.edit_accesscode1 = QLineEdit()
        self.edit_accesscode1.setEchoMode(QLineEdit().Password)        
        self.edit_accesscode2 = QLineEdit()        
        self.edit_accesscode2.setEchoMode(QLineEdit().Password)        
        
        Next_button= QPushButton("&Next")
        Next_button.clicked.connect(self.pass_accesscode)
        Back_button= QPushButton("&Back")
        Back_button.clicked.connect(self.back_page)

        titlebox.addWidget(label_text)

        hbox_bottom.addWidget(Back_button)
        hbox_bottom.addStretch()
        hbox_bottom.addWidget(Next_button)

        edit_form.addRow(label_accesscode1,self.edit_accesscode1)
        edit_form.addRow(label_accesscode2,self.edit_accesscode2)

        form_box.addSpacing(5)
        form_box.addLayout(edit_form)
        form_box.addSpacing(Next_button.sizeHint().width()+6)
        
        layout.addStretch()
        layout.addLayout(titlebox)
        layout.addStretch()
        layout.addLayout(form_box)
        layout.addLayout(hbox_bottom)

        self.setLayout(layout)

    def pass_accesscode(self):
        self.parent.connectpd(self.edit_accesscode1.text(),self.edit_accesscode2.text())

    def back_page(self):
        self.parent.init_ui()
