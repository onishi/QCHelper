import sys
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sip

# from PyQt5.QtWidgets import *
# from PyQt5.QtGUI import *
# from PyQt5.QtCore import *

class InitialWindow(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__(parent)
        self.parent = parent

        self.target_type=''

        label_title = QLabel()
#        label_title.setText('<center><font size="7">Welcome to QC Database tool!!</font></center>')
        label_title.setText('<center><font size="7">to be replaced that entry of SerialNo.</font></center>')
        label_notice = QLabel()
        label_notice.setText('<center><font size="6">Select your test target</font></center>')
        
        titlebox= QVBoxLayout()
        vbox_whole = QVBoxLayout()
        hbox_bottom = QHBoxLayout()
        radio_vbox = QVBoxLayout()
        radio_box = QHBoxLayout()
                
        Next_button= QPushButton("&Next")
        Next_button.clicked.connect(self.pass_moduleid)
        
        radio_list =[]
        radio_group = QButtonGroup()        
        for i,test_title in enumerate( self.parent.GUI_dict.keys() ): 
            radio_list.append( self.make_radiobutton(test_title) )
            radio_list[i].toggled.connect(lambda:self.choice_target(radio_list[i]))
            radio_group.addButton(radio_list[i],i)
            radio_vbox.addWidget(radio_list[i])

        radio_box.addStretch()
        radio_box.addLayout(radio_vbox)
        radio_box.addStretch()

        titlebox.addWidget(label_title)
        titlebox.addWidget(label_notice)

        hbox_bottom.addStretch()
        hbox_bottom.addWidget(Next_button)

        vbox_whole.addStretch()
        vbox_whole.addLayout(titlebox)
        vbox_whole.addStretch()
        vbox_whole.addLayout(radio_box)
        vbox_whole.addStretch()
        vbox_whole.addLayout(hbox_bottom)
        self.setLayout(vbox_whole)
        
    def pass_moduleid(self):

        if self.target_type in self.parent.GUI_dict.keys():
            self.parent.call_targetGUI(self.target_type)
        else:
            QMessageBox.warning(None, 'Worning','Select your test target', QMessageBox.Ok)
            self.parent.init_ui()

    def make_radiobutton(self,label):
        radiobutton = QRadioButton(label)
        radiobutton.setCheckable(True)
        radiobutton.setFocusPolicy(Qt.NoFocus)
        radiobutton.setStyleSheet('QRadioButton{font: 25pt Helvetica MS;} QRadioButton::indicator { width: 30px; height: 30px;};')
        return radiobutton

    def choice_target(self,button):
        self.target_type = button.text()
        
    
