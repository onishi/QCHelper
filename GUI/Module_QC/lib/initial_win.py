import sys
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sip

# from PyQt5.QtWidgets import *
# from PyQt5.QtGUI import *
# from PyQt5.QtCore import *

class InitialWindow(QWidget):
    def __init__(self, parent=None):
#        super(QWidget, self).__init__(parent)
        super(QWidget, self).__init__()
        self.parent = parent
        
        layout = QGridLayout()
        
        label_text = QLabel()
        label_text.setText('<center><font size="7">Welcome to QC Helper!!</font></center>')
        label_atlsn = QLabel()
        label_atlsn.setText('ATLAS Serial Number:')

        self.edit_atlsn = QLineEdit()
        self.edit_atlsn.setText(self.parent.atlsn)
        
        button= QPushButton("&Next")
        button.clicked.connect(self.pass_atlsn)

        self.check_practice = QCheckBox()
        self.check_practice.setText('practice mode')
        if self.parent.isPractice:
            self.check_practice.setCheckState(Qt.Checked)
        


        layout.addWidget(label_text,0,1)
        layout.addWidget(label_atlsn,1,0)
        layout.addWidget(self.edit_atlsn,1,1)
        layout.setRowStretch(2,2)
        layout.addWidget(self.check_practice,2,1,Qt.AlignRight)
        layout.addWidget(button,2,2)
        self.setLayout(layout)

    def pass_atlsn(self):
        if self.check_practice.checkState()==Qt.Checked:
            self.parent.isPractice = True
            print( 'practice mode')
        else:
            self.parent.isPractice = False
            
        self.parent.recieve_atlsn(self.edit_atlsn.text())
