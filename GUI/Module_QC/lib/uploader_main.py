import os
import sys
import traceback
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sip
import json
from bson.objectid import ObjectId
import pprint
#import datetime
from datetime import date, datetime


sys.path.append(os.path.join(os.path.dirname(__file__), '../../../dbinterface/lib'))
import db_retriever

sys.path.append(os.path.join(os.path.dirname(__file__), './'))
#import ConfirmWindow
from ConfirmWindow import *

#import default_default
#import Module_default

class UploadWindow(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__(parent)
        self.parent = parent

        self.DB_IPaddress = self.parent.DB_IPaddress
        self.DB_port = self.parent.DB_port
        self.db_dict = self.parent.db_dict 
        self.col_dict = self.parent.col_dict
        self.result_dict = self.parent.result_dict
        self.device_type = self.parent.device_type

        self.moduleid = self.parent.moduleid
        self.stage = self.parent.stage
        self.testtype = self.parent.testtype
 

        target_and_test = self.device_type +'_' +self.result_dict['testname'] + '.ConfirmWindow(self)'
        target_and_default = self.device_type +'_default.ConfirmWindow(self)'
        defalutLayout = 'default_layout.ConfirmWindow(self)'
        try:
            self.confirm_wid = eval( target_and_test )
        except:
            try:
                self.confirm_wid = eval( target_and_default )
            except:
                print (traceback.format_exc() )
                try:
                    self.confirm_wid = eval( defalutLayout )
                except:
                    print (traceback.format_exc() )
                    QMessageBox.warning(None, 'Worning','Module:"default_layout.py" has been removed', QMessageBox.Ok)
                    sys.exit()
        label_title = QLabel()
        label_title.setText('<center><font size="7">Please Confirm before upload to database</font></center>')
        label_text = QLabel()
        label_text.setText('<center><font size="5">Device type : ' + self.parent.device_type + '</font></center>')

        Upload_button= QPushButton("&Upload!")
        Upload_button.clicked.connect(self.upload_to_db)
        json_button= QPushButton("&Check json (for expert)")
        json_button.clicked.connect(self.check_json)
#        json_button.clicked.connect(self.open_json)

        titlebox= QVBoxLayout()        
        layout = QVBoxLayout() 
        hbox_bottom = QHBoxLayout()

        titlebox.addWidget(label_title)
        titlebox.addWidget(label_text)

        hbox_bottom.addStretch()
        hbox_bottom.addWidget(json_button)
        hbox_bottom.addWidget(Upload_button)

        inner = QScrollArea()
        inner.setWidgetResizable(True)
        inner.setWidget(self.confirm_wid)

        layout.addLayout(titlebox)
        layout.addWidget(inner)
        layout.addLayout(hbox_bottom)

        self.setLayout(layout)

    def Widget_return(self):
        return self.confirm_wid
    
    def check_json(self):
        self.parent.confirm_json()
    
    def open_json(self):
        self.confirm_wid = json_layout.JsonWindow(self)
        return self.confirm_wid

    def back_to_result(self):
        self.parent.comfirm_result()
    
    def upload_to_db(self):
        self.parent.upload_to_db()

        
