import os
import shutil
import sys
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sip
import cv2
import numpy as np
import json
import pymongo
#import datetime
from datetime import date, datetime
from bson.objectid import ObjectId


sys.path.append(os.path.join(os.path.dirname(__file__), '../../../dbinterface/lib'))
import db_retriever
import db_uploader

sys.path.append(os.path.join(os.path.dirname(__file__), '../../ASICQC/bin'))
#import ASICQC
sys.path.append(os.path.join(os.path.dirname(__file__), '../../HybridQC/bin'))
#import HybridQC
sys.path.append(os.path.join(os.path.dirname(__file__), '../../ModuleQC/bin'))
import ModuleQC

sys.path.append(os.path.join(os.path.dirname(__file__), '../lib'))
import initial_main
import uploader_main
#import register_win
#import choosefile_win
#import loadimg_win
#import trimimg_win
#import splitimg_win
#import inspection_win
#import summary_win

class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.setGeometry(0, 0, 900, 500)

        #database IP
        self.IPaddress=''
        #database port
        self.port=''
        #database name
        self.DBname = ''
        #collection name
        self.module_col = ''
        self.status_col = ''
        self.test_col = ''

        #result container
        self.result_dict = {
            'test':'test'
        }
        self.ideal_result_dict = self.result_dict.copy()



        '''
        self.moduleid = ''
        self.inspector = ''
        self.stage = ''
        self.institute = ''
        self.testtype = ''
        '''        
        '''
        # checked page list
        self.tot_page = 64
        self.page_checked = []
        for i in range(self.tot_page):
        # self.page_checked = {'page{}'.format(i):False}
        self.page_checked.insert(i,False)            
        '''
        
        # dict of target GUI
        self.GUI_dict = {
            'ASIC':'ASICQC',
            'Hybrid':'HybridQC',
            'Module':'ModuleQC'
        }
        #GUI name of test target
        self.targetGUI=''

        '''
        # User list
        self.user_list=[]
        # Institutex list
        self.user_list=[]
        '''
        self.setWindowTitle('QC database interface tool')
                
        self.init_ui()
        
    def init_ui(self):

        self.initial_wid = initial_main.InitialWindow(self)
        self.update_widget(self.initial_wid)
        
    def update_widget(self, w):
        self.setCentralWidget(w)
        self.show()


    def call_targetGUI(self,which_GUI):
        self.targetGUI = self.GUI_dict[which_GUI] + '.self.GUI_dict[which_GUI]' + 'Window(self)'

        #        self.targetGUI_wid = ModuleQC.ModuleQCWindow(self)
        #        self.targetGUI_wid = ModuleQC.ModuleQCWindow(self)

        #        self.targetGUI_wid = exec(self.targetGUI)
        #        self.update_widget(self.targetGUI_wid)
        
        self.result_dict = self.temporary_create_dict()

        self.recieve_result( self.result_dict )
        

    def recieve_result(self,result):
        self.result_dict = self.trans_json_to_dict(result)

        self.upload_wid = uploader_main.UploadWindow(self)
        self.update_widget(self.upload_wid)


        '''    
    def upload_to_db(self,data_dict):
        #        self.trans_json_to_dict(data_dict):

        if self.module_isRegistered:
            localdb_uploader.uploader('visual_inspection',dic_json)        
            localdb_uploader.updater('visual_inspection',self.moduleid,self.stage,self.img_list)
            localdb_uploader.find_and_show('visual_inspection',self.moduleid,self.stage)
        else:
            with open('temp/result.json', 'w') as f:
                json.dump(dic_json, f, indent=4)
        
        # try:
        #     localdb_uploader.updater('visual_inspection',self.moduleid,self.stage,self.img_list)
        # except NameError:
        #     print('Image does not exist. Finish Inspection')
        '''








    
        '''
    #    def recieve_moduleid(self, mod_id, stage, inspector,institute,TestType):
    def recieve_moduleid(self, mod_id, inspector,institute):
        self.moduleid = mod_id
#        self.stage = stage
        self.inspector = inspector
        self.institute = institute
#        self.testtype = TestType
        
        self.reg_wid = register_win.RegModWindow(self)
        self.update_widget(self.reg_wid)

    def open_file_win(self):
        self.file_wid = choosefile_win.ChooseFileWindow(self)
        self.update_widget(self.file_wid)
        
    def load_img(self):
        try:
            self.img_bgr            
            self.load_img_wid = loadimg_win.LoadImageWindow(self)
            self.update_widget(self.load_img_wid)
        except AttributeError as e:
            options = QFileDialog.Options()
            inputFileName, _ = QFileDialog.getOpenFileName(self,'Open File','','Image files(*.jpg *.png)',options=options)
            if not inputFileName:
                print('image is not choosed')
            else:
                self.statusBar().showMessage(inputFileName)
                print(inputFileName)
            
                self.img_bgr = cv2.imread(inputFileName,1)
                self.scale = 10
                self.n_page = 0
                print('OpenCV image read success.')

                self.load_img_wid = loadimg_win.LoadImageWindow(self)
                self.update_widget(self.load_img_wid)

    def trim_img(self):
        self.trim_img_wid = trimimg_win.TrimImageWindow(self)
        self.update_widget(self.trim_img_wid)

    def split_img(self):
        self.split_img_wid = splitimg_win.SplitImageWindow(self)
        self.update_widget(self.split_img_wid)        

    def inspection(self):
        inspection_wid = inspection_win.InspectionWindow(self)        
        self.update_widget(inspection_wid)

    def write_to_localdb(self):
        dic = {
            "component_name":self.moduleid,
            "inspector":self.inspector,
            "stage":self.stage,
            "anomaly":self.anomaly_list,
            "comment":self.comment_list,
            "img":self.img_list
        }
        dic_json = json.dumps(dic)

        if self.module_isRegistered:
            localdb_uploader.uploader('visual_inspection',dic_json)        
            localdb_uploader.updater('visual_inspection',self.moduleid,self.stage,self.img_list)
            localdb_uploader.find_and_show('visual_inspection',self.moduleid,self.stage)
        else:
            with open('temp/result.json', 'w') as f:
                json.dump(dic_json, f, indent=4)
        
        # try:
        #     localdb_uploader.updater('visual_inspection',self.moduleid,self.stage,self.img_list)
        # except NameError:
        #     print('Image does not exist. Finish Inspection')

    def go_to_summary(self):
        summary_wid = summary_win.SummaryWindow(self)
        self.update_widget(summary_wid)

        '''

    def temporary_create_dict(self):
        temp_dict={
            "_id" : ObjectId("5ef4285e21ee73ab61c76da1"),
            "component" : "5ef18bdd2b8b06e1bc976d7d",
            "user_id" : "onishi",
            "address" : "osaka",
            "stage" : "localDB_practice",
            "sys" : {
                #                "cts" : ISODate("2020-06-25T13:00:00Z"),
                #                "mts" : ISODate("2020-06-25T13:00:00Z"),
#                "cts" : datetime.datetime.now(),
#                "mts" : datetime.datetime.now(),
               "cts" : datetime.now(),
                "mts" : datetime.now(),
                "rev" : 1
            },
            "testname" : "MASS",
            "results" : {
                "dbVersion" : "0.01",
                "Weight" : "1000"
            }
        }
        return temp_dict
        
    def trans_json_to_dict(self,trans_file):
        if type(trans_file) is str:
            return json.loads(trans_file)
        elif type(trans_file) is dict:
            return trans_file
        else:
            QMessageBox.warning(None, 'Worning','result should be <dict> class', QMessageBox.Ok)



if __name__ == '__main__':
    app = QApplication(sys.argv)
    main_window = MainWindow()
    main_window.show()
    sys.exit(app.exec_())
            


