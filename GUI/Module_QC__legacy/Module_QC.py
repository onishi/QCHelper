import os
import shutil
import sys
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sip
import cv2
import numpy as np
import json
import pymongo
from bson.objectid import ObjectId
from importlib import import_module
from importlib import machinery

sys.path.append(os.path.join(os.path.dirname(__file__), '../../../dbinterface/lib'))
import localdb_uploader

sys.path.append(os.path.join(os.path.dirname(__file__), './lib'))
import initial_Module
import choosetest_Module


class Module_QC_Window(QMainWindow):
############################################################################################
    def __init__(self, parent=None):
        super(QMainWindow, self).__init__(parent)
        self.parent = parent

        #copy result dict
        self.result_dict = self.parent.result_dict.copy()

        self.localDBname = self.parent.db_dict[self.parent.device_type]
        self.module_col = self.parent.col_dict[self.parent.device_type]['module_info']
        self.user_col = self.parent.col_dict[self.parent.device_type]['user_info']
        self.institute_col = self.parent.col_dict[self.parent.device_type]['institute_info']
        self.test_col = self.parent.col_dict[self.parent.device_type]['testResult']

        #self.serial = self.result_dict['serialNumber']
        #self.serial = 'OU076A'       ###temporary
        self.result_dict['serialNumber'] = 'OU076A'          ###temporary

        self.setGeometry(0, 0, 900, 500)
        self.setWindowTitle('Module QC Helper')

############################################################################################
    def init_ui(self):
        self.initial_wid = initial_Module.InitialWindow(self)
        self.update_widget(self.initial_wid)

    def update_widget(self, w):
        self.setCentralWidget(w)
        self.show()

    def close_and_return(self):
        self.close()
        self.parent.show()
#        self.parent.recieve_return(self)       

    def back_page(self):
        self.parent.init_ui()

    def recieve_backpage(self):
        pass

    def call_another_window(self,window):
        self.hide()
        window.init_ui()

    def return_result(self):
        self.parent.recieve_result(self,self.result_dict)

    def recieve_info(self,user_name,institute):
        self.result_dict['user_id']=user_name
        self.result_dict['address']=institute
        
        self.choose_which_test()

    def choose_which_test(self): 
        self.choosetest_wid = choosetest_Module.ChooseWindow(self)
        self.update_widget(self.choosetest_wid)
       
    def call_each_test(self):
        testGUI = os.path.join(os.path.dirname(__file__), '../../each_test/' + self.result_dict['testname'] + '/' + self.result_dict['testname'] +'.py' )

        self.test_module = machinery.SourceFileLoader( self.result_dict['testname'] , testGUI).load_module()
        test_class =  str('self.test_module.' + self.result_dict['testname'] + '_Window(self)' )

        self.test_win = eval( test_class )
        self.call_another_window(self.test_win)

    def recieve_result(self,window,result_dict):
        self.latest_page = winodow
        self.latest_page.hide()

        result = self.trans_json_to_dict(result_dict)

        self.result_dict.update(result)
        self.return_result()

    def trans_json_to_dict(self,trans_file):
        try:
            return json.load(trans_file)
        except:
            try:
                return json.loads(trans_file)
            except:
                if type(trans_file) is dict:
                    return trans_file
                else:
                    QMessageBox.warning(None, 'Worning','result should be <dict> class', QMessageBox.Ok)

#        if type(trans_file) is str:
#            return json.loads(trans_file)
#        elif type(trans_file) is dict:
#            return trans_file
#        else:
#            QMessageBox.warning(None, 'Worning','result should be <dict> class', QMessageBox.Ok)

