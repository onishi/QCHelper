import sys, os
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sip
import pymongo
from bson.objectid import ObjectId
import traceback


sys.path.append(os.path.join(os.path.dirname(__file__), '../../dbinterface/lib'))
#import localdb_retriever
import db_retriever

class ChooseWindow(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__(parent)
        self.parent = parent

        next_button = QPushButton('&Next')
        next_button.clicked.connect(self.next_page)
        back_button = QPushButton('&Back')
        back_button.clicked.connect(self.back_page)

        button_box = QHBoxLayout()
        button_box.addWidget(back_button)
        button_box.addStretch()
            

        try:
            #get ObjectID of module component
            self.parent.result_dict['component'] = str( db_retriever.localdb_retriever(self.parent.localDBname,self.parent.module_col,{'serialNumber':self.parent.result_dict['serialNumber']},'_id') )
            self.parent.module_isRegistered = True
            print(self.parent.result_dict['component'] )

            #get current stage
#            self.parent.result_dict['stage'] = db_retriever.localdb_retriever(self.parent.localDBname,self.parent.module_col,{'component':self.parent.result_dict['component']},'current_stage')
            self.parent.result_dict['stage'] = 'test'           ############temporary
            self.test_dict = self.Fill_test_dict(self.parent.result_dict['stage'])
            
            label_title = QLabel()
            label_title.setText('<center><font size="7"> Which test do you want to do?</font></center>')

            layout = QVBoxLayout()            
            titlebox= QVBoxLayout()
            radio_box = QFormLayout()
            radio_Hbox = QHBoxLayout()            
            info_layout = self.make_layout(self.parent.result_dict)
        
            self.radiobutton_list = []
            self.testlabel_list = []
            self.radio_group = QButtonGroup()

            for i,test_title in enumerate( self.test_dict.keys() ):                
                self.radiobutton_list.append(self.make_radiobutton(test_title))
                self.radio_group.addButton(self.radiobutton_list[i],i)
                self.testlabel_list.append( self.check_uploaded( self.test_dict[test_title] ) )
                radio_box.addRow(self.testlabel_list[i],self.radiobutton_list[i])

            titlebox.addWidget(label_title)

            button_box.addWidget(back_button)
            button_box.addStretch()
            button_box.addWidget(next_button)
            
            radio_Hbox.addStretch()
            radio_Hbox.addLayout(radio_box)
            radio_Hbox.addStretch()

            
            layout.addStretch()
            layout.addLayout(titlebox)
            layout.addLayout(info_layout)
            layout.addStretch()
            layout.addLayout(radio_Hbox)
            layout.addStretch()
            layout.addLayout(button_box)
            self.setLayout(layout)


        # Case for module is not registered in the local DB
        except TypeError:
            import traceback
            traceback.print_exc()            
            label_notregistered_text = QLabel()
            label_notregistered_text.setText('<center><font size="7">Module \''+self.parent.result_dict['serialNumber']+'\' is not registered in Local DB. \n Please register it.</font></center>')

            self.parent.module_isRegistered = False
            
            layout_notregistered = QVBoxLayout()

            layout_notregistered.addStretch()
            layout_notregistered.addWidget(label_notregistered_text)

            layout_notregistered.addStretch()
            layout_notregistered.addLayout(button_box)
            self.setLayout(layout_notregistered)

        # Case for being not able to connect local DB
        except pymongo.errors.ServerSelectionTimeoutError:

            label_nolocalDB_text = QLabel()
            label_nolocalDB_text.setText('<center><font size="7">Cannot connect to the localDB. Please check database name and portNo.</font></center>')

            self.parent.module_isRegistered = False            

            layout_nolocalDB = QVBoxLayout()
            layout_nolocalDB.addStretch()
            layout_nolocalDB.addWidget(label_nolocalDB_text)
            layout_nolocalDB.addStretch()
            layout_nolocalDB.addWidget(button_box)

            self.setLayout(layout_nolocalDB)

 

            
    def next_page(self):
        self.choice_target()       
        self.parent.call_each_test()
        
    def back_page(self):
        self.parent.init_ui()        

    
    def check_uploaded(self,test_to_check):

        try:
            db_retriever.localdb_retriever(self.parent.localDBname,self.parent.test_col,{'component':self.parent.result_dict['component'],'stage':self.parent.result_dict['stage'],'testname':test_to_check  },'_id')
            Text = '<font color="red">already uploaded</font>'
        except TypeError:
            import traceback
            traceback.print_exc()
            Text=''

        return Text

    def make_radiobutton(self,label):
        radiobutton = QRadioButton(label)
        radiobutton.setCheckable(True)
        radiobutton.setFocusPolicy(Qt.NoFocus)
        radiobutton.setStyleSheet('QRadioButton{font: 20pt;} QRadioButton::indicator { width: 25px; height: 25px;};')
        
        if label == self.parent.result_dict['testname']:
            radiobutton.setChecked(True)
        return radiobutton


    def choice_target(self):
        self.parent.result_dict['testname'] = self.test_dict[ self.radio_group.checkedButton().text() ]
        

    def add_Formlayout(self,Form_layout,label_str,form_text):

        label = QLabel()
        label.setText(label_str)

        editor = QLineEdit()
        editor.setText(form_text)
        editor.setReadOnly(True)
        editor.setStyleSheet("background-color : linen")
        Form_layout.addRow(label,editor)

    def make_layout(self,result_dict):
        try:
            user_text = db_retriever.localdb_retriever(self.parent.localDBname,self.parent.user_col,{ '_id' : ObjectId(result_dict['user_id']) },'userName')
        except:
            print (traceback.format_exc() )
            user_text = 'failed to find in localDB'
        try:
            institute_text = db_retriever.localdb_retriever(self.parent.localDBname,self.parent.institute_col,{ '_id' : ObjectId(result_dict['address']) },'institution')
        except:
            print (traceback.format_exc() )
            institute_text = 'failed to find in localDB'
        try:
            serial_text = db_retriever.localdb_retriever(self.parent.localDBname,self.parent.module_col,{ '_id' : ObjectId(result_dict['component']) },'serialNumber')
        except:
            print (traceback.format_exc() )
            serial_text = 'failed to find in localDB'

        Form_layout = QFormLayout()
        layout = QHBoxLayout()
        self.add_Formlayout(Form_layout,'Serial Number',serial_text)
        self.add_Formlayout(Form_layout,'test stage',result_dict['stage'])
        self.add_Formlayout(Form_layout,'your name',user_text)
        self.add_Formlayout(Form_layout,'your institute',institute_text)

        layout.addStretch()
        layout.addLayout(Form_layout)
        layout.addStretch()
        
#        return Form_layout
        return layout









    def Fill_test_dict(self,current_stage):
        
        test_dict = {
            'MODULEBARE':{
                'Optical_Inspection':'OPTICAL',
                'Mass_Measurement':'MASS',
                'Bare_Module_Metrology':'METROLOGY_BARE'
            },
            'MODULETOPCB':{
                'Optical_Inspection':'OPTICAL',
                'Mass_Measurement':'MASS',
                'Module+Flex_Attach_Metrology':'METROLOGY_FLEX_ATTACH',
                'Glue_Information_Module+Flex_Attach':'GLUE_MODULE_FLEX_ATTACH'
            },
            'MODULEWIREBONDING':{
                'Optical_Inspection':'OPTICAL',
                'Wirebonding_Information':'WIREBONDING',
                'Wirebond_pull_tests':'WIREBOND',
                'IV_Curve':'IV_CURVE',
                'Full_Electrical_Test':'FULL_ELECTRICAL_TEST'
            },
            'MODULEWIREBONDPROTECTION':{
                'Optical_Inspection':'OPTICAL',
                'Mass_Measurement':'MASS',
                'Glue_Information_Potting':'POTTING',
                'IV Curve':'IV_CURVE',
                'Basic Electrical Tests':'BASIC_ELECTRICAL'
            },
            'MODULEPARYLENECOATING':{
                'Optical_Inspection':'OPTICAL',
                'Mass_Measurement':'MASS',
                'Parylene Properties':'PARYLENE',
                'IV Curve':'IV_CURVE',
                'Basic Electrical Tests':'BASIC_ELECTRICAL'
            },
            'MODULETHERMALCYCLING':{
                'Optical_Inspection':'OPTICAL',
                'IV Curve':'IV_CURVE',
                'Basic Electrical Tests':'BASIC_ELECTRICAL',
                'Thermal Cycling':'THERMAL_CYCLING'
            },
            'MODULEBURNIN':{
                'Optical_Inspection':'OPTICAL',
                'Mass_Measurement':'MASS',
                'Final Module Metrology':'METROLOGY_FINAL',
                'IV_Curve':'IV_CURVE',
                'Full_Electrical_Test':'FULL_ELECTRICAL_TEST'
            },
            'localDB_practice':{
                'Optical_Inspection':'OPTICAL',
                'Mass_Measurement':'MASS',
            },
            'test':{
                'Optical_Inspection':'OPTICAL',
                'Mass_Measurement':'MASS',
                'Parylene Properties':'PARYLENE',
                'Thermal Cycling':'THERMAL_CYCLING',
                'IV Curve':'IV_CURVE',
                'Full_Electrical_Test':'FULL_ELECTRICAL_TEST',
                'Final Module Metrology':'METROLOGY_FINAL',
            }
        }
        test_dict_error = {
            'Not_applicable':'None'
        }

        try:
            return test_dict[current_stage]
        except Exception as e:
            return test_dict_error
            
