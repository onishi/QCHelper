import sys
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sip

# from PyQt5.QtWidgets import *
# from PyQt5.QtGUI import *
# from PyQt5.QtCore import *

class InitialWindow(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__(parent)
        self.parent = parent

        layout = QVBoxLayout()
        titlebox = QVBoxLayout()
        edit_box = QFormLayout()
        bottom_box = QHBoxLayout()


        label_text = QLabel()
        label_text.setText('<center><font size="7">module QC tool</font></center>')
        label_user = QLabel()
        label_user.setText('your name:')
        label_institute = QLabel()
        label_institute.setText('your institute:')

        back_button= QPushButton("&Back(to be developed)")
        back_button.clicked.connect(self.back_page)
        next_button= QPushButton("&Next")
        next_button.clicked.connect(self.next_page)

        self.edit_user = QLineEdit()    #### may change to select box?? 
        self.edit_institute = QLineEdit()

        titlebox.addWidget(label_text)

        edit_box.addRow(label_user,self.edit_user)
        edit_box.addRow(label_institute,self.edit_institute)

        bottom_box.addWidget(back_button)
        bottom_box.addStretch()
        bottom_box.addWidget(next_button)

        layout.addLayout(titlebox)
        layout.addStretch()
        layout.addLayout(edit_box)
        layout.addLayout(bottom_box)
        self.setLayout(layout)


    def back_page(self):
#        self.parent.back_page()
        self.parent.close_and_return()

    def next_page(self):
        self.parent.recieve_info( self.edit_user.text(),self.edit_institute.text() )

    def get_user_list(self):
        #retrieve use and institute list from localDB
        


#above all now
################################################################

        '''
        titlebox= QVBoxLayout()
        vbox = QVBoxLayout()
        hbox = QHBoxLayout()
        layout = QFormLayout()
        
        label_text = QLabel()
        label_text.setText('<center><font size="7">Welcome to localDB uploader!!</font></center>')
        label_inspector = QLabel()
        label_inspector.setText('Inspector:')
        label_moduleid = QLabel()
        label_moduleid.setText('Module ID:')
        #        label_stage = QLabel()
        #        label_stage.setText('Stage:')
        label_institute = QLabel()
        label_institute.setText('Institute:')
        #        label_whichtest = QLabel()
        #        label_whichtest.setText('Test type:')
        
        self.edit_inspector = QLineEdit()
        self.edit_inspector.setText(self.parent.inspector)
        self.edit_inspector.setMaxLength(50)

        self.edit_moduleid = QLineEdit()
        self.edit_moduleid.setText(self.parent.moduleid)
        self.edit_moduleid.setMaxLength(50)
        
        #        self.edit_stage = QLineEdit()
        #        self.edit_stage.setText(self.parent.stage)
        #        self.edit_stage.setMaxLength(50)
        
        self.edit_institute = QLineEdit()
        self.edit_institute.setText(self.parent.institute)
        self.edit_institute.setMaxLength(50)

        button= QPushButton("&Next")
        button.clicked.connect(self.pass_moduleid)

        #        self.choose_test=QComboBox()
        #        self.choose_test.addItem('------ Select test type -----')        
        #        self.choose_test.addItems(self.parent.test_list)
        
        self.checkbox_wo_upload = QCheckBox('Only prepare json file w/o uplording to localDB (to be developed)')
        self.checkbox_wo_upload.stateChanged.connect(self.change_wo_upload_flag)
        
        self.checkbox_practice = QCheckBox('Practice mode (to be developed)')
        self.checkbox_practice.stateChanged.connect(self.change_LineEdit)
        
        #        self.check_group = QButtonGroup()
        #        self.check_group.addButton(self.checkbox_wo_upload,1)
        #        self.check_group.addButton(self.checkbox_practice,2)
        

        if self.parent.wo_upload==True:
            self.checkbox_practice.setCheckState(Qt.Checked)

        titlebox.addWidget(label_text)

        ### input Form ###
        layout.addRow(label_inspector,self.edit_inspector)
        layout.addRow(label_institute,self.edit_institute)
        layout.addRow(label_moduleid,self.edit_moduleid)
        #        layout.addRow(label_stage,self.edit_stage)
        #        layout.addRow(label_whichtest,self.choose_test)
        ##################

        hbox.addStretch()
        hbox.addWidget(self.checkbox_wo_upload)
        hbox.addWidget(self.checkbox_practice)
        hbox.addWidget(button)

        vbox.addStretch()
        vbox.addLayout(titlebox)
        vbox.addStretch()
        vbox.addLayout(layout)
        vbox.addLayout(hbox)
        self.setLayout(vbox)
        '''

    def pass_moduleid(self):
#        if self.choose_test.currentText()=="------ Select test type -----":
#            QMessageBox.warning(None, 'Worning','Please Fill all box', QMessageBox.Ok)
#            self.parent.init_ui()
#        elif self.edit_moduleid.text()=="": 
        if self.edit_moduleid.text()=="": 
            QMessageBox.warning(None, 'Worning','Please Fill all field', QMessageBox.Ok)
            self.parent.init_ui()
#        elif self.edit_stage.text()=="":
#            QMessageBox.warning(None, 'Worning','Please Fill all box', QMessageBox.Ok)
#            self.parent.init_ui()
        elif self.edit_inspector.text()=="":
            QMessageBox.warning(None, 'Worning','Please Fill all field', QMessageBox.Ok)
            self.parent.init_ui()
        elif self.edit_institute.text()=="":
            QMessageBox.warning(None, 'Worning','Please Fill all field', QMessageBox.Ok)
            self.parent.init_ui()

        else:
            #self.parent.recieve_moduleid(self.edit_moduleid.text(),self.edit_stage.text(),self.edit_inspector.text(),self.edit_institute.text(),self.choose_test.currentText())
            self.parent.recieve_moduleid(self.edit_moduleid.text(),self.edit_inspector.text(),self.edit_institute.text())
         


    def change_LineEdit(self):
        if self.checkbox_practice.checkState() == Qt.Checked:


            self.parent.inspector = self.checkbox_disable(self.edit_inspector,'test_inspector')
            self.parent.moduleid = self.checkbox_disable(self.edit_moduleid,'test_moduleid')
            self.parent.institute = self.checkbox_disable(self.edit_institute,'test_institute')

            self.parent.practice = True

        else:

            self.checkbox_enable(self.edit_inspector,self.parent.inspector)
            self.checkbox_enable(self.edit_moduleid,self.parent.moduleid)
            self.checkbox_enable(self.edit_institute,self.parent.institute)

            self.parent.practice = False


    def checkbox_enable(self,lineEdit,Text):

        lineEdit.setText(Text)
        lineEdit.setReadOnly(False)
        lineEdit.setStyleSheet("background-color : white")
        
    def checkbox_disable(self,lineEdit,Text):

        preText = lineEdit.text()        
        lineEdit.setText(Text)
        lineEdit.setReadOnly(True)
        lineEdit.setStyleSheet("background-color : lightgray")
        
        return preText





    def change_wo_upload_flag(self):
        if self.checkbox_wo_upload.checkState() == Qt.Checked:
            self.parent.wo_upload = True        
        else:
            self.parent.wo_upload = False
        
