import os
import shutil
import sys
import traceback
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sip
import cv2
import numpy as np
import json
import pymongo
from importlib import import_module
from importlib import machinery
import itkdb

#import datetime
from datetime import date, datetime
from bson.objectid import ObjectId


sys.path.append(os.path.join(os.path.dirname(__file__), './dbinterface/lib'))
import db_retriever
import db_uploader

import localdb_uploader
import localdb_retriever
import localdb_authenticator

#sys.path.append(os.path.join(os.path.dirname(__file__), './GUI/Main_Window/lib'))
sys.path.append(os.path.join(os.path.dirname(__file__), './GUI/Module_QC/lib'))
#import initial_main
#import uploader_main

import initial_win
import connectpd_win
import componentinfo_win
import connectdb_win
import choosetest_win

class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.setGeometry(0, 0, 900, 500)

        #################################################
        ###### developper have to handle in Widget ######
#        self.isOK_to_upload = False
        self.isOK_to_upload = True
        #result container recommended 'dict' 
        self.result_dict = self.init_result_dict()
        #################################################

        #####################################################
        ###### developper can handle the practice flag ######
        #practice mode flag
        self.isPractice = False
#        self.isPractice = True
        #####################################################

        ######################################################
        ###### developer can use it to manage the data ######
        self.DBname = 'localdb'

        self.col_dict = {
            'module_info':'component',
            'user_info':'user',
            'institution_info':'institution',
            'testResult':'QC.result'
        }

        ######################################################

        '''

        # checked page list
        self.tot_page = 64
        self.page_checked = []
        for i in range(self.tot_page):
        # self.page_checked = {'page{}'.format(i):False}
        self.page_checked.insert(i,False)            
        '''
        ###########################################################
        #GUI name of test target
#        self.device_type=''
        self.setWindowTitle('QC Helper')        
        self.atlsn = ''
        self.type_name = ''
        self.db_user = ''
        self.db_pass = ''
        ###########################################################

        self.init_ui()
        
    def init_ui(self):
        self.initial_wid = initial_win.InitialWindow(self)
        self.update_widget(self.initial_wid)
        
    def update_widget(self, w):
        self.setCentralWidget(w)
        self.show()

    def close_window(self):
        self.close()

    def call_another_window(self,window):
        self.hide()
        window.init_ui()

    def back_to_last_window(self,window):
        self.hide()
        window.show()

    def recieve_return(self,window):
        window.hide()
        self.show()

    def recieve_atlsn(self, sn):
        
        self.atlsn = sn
        self.connectpd_wid = connectpd_win.ConnectPDWindow(self)
        self.update_widget(self.connectpd_wid)
        print(sn)

    def connectpd(self,code1,code2):
        token = self.process_request(code1,code2)

        if token == 0:
            QMessageBox.warning(None, 'Worning','Wrong AccessCode', QMessageBox.Ok)
#            sys.exit(1)
        else:
            try:
                u = itkdb.core.User(accessCode1=code1, accessCode2=code2)
                pd_client = itkdb.Client(user=u)
                self.doc = pd_client.get('getComponent', json={"project": "P", "component": self.atlsn})
                self.type_name = self.doc["componentType"]["code"]
                print(self.type_name)

                self.result_dict['component'] = self.atlsn
                self.result_dict['componentType'] = self.type_name
                self.result_dict['institution'] = self.doc["institution"]["name"]
                self.result_dict['currentLocation'] = self.doc["currentLocation"]["name"]
                self.result_dict['type'] = self.doc["type"]["name"]
                self.result_dict['currentStage'] = self.doc["currentStage"]["name"]
                self.result_dict['date'] = datetime.now()
                self.result_dict['version'] = 0.01
            
                self.see_info()
            except:
                print (traceback.format_exc() )
                QMessageBox.warning(None, 'Worning','component:'+self.atlsn+'is not found in ITkPD', QMessageBox.Ok)
                self.init_ui()

    def process_request(self,code1,code2):
        try:
            u = itkdb.core.User(accessCode1=code1, accessCode2=code2)
            pd_client = itkdb.Client(user=u)
            docs = pd_client.get('listInstitutions', json={})
            print('Authorized.')
            request = 1
        except:
            print('Not authorized. Please login for ITkPD by using itkpd-interface/authenticate.sh')
            request = 0
        return request

    def see_info(self):
        self.componentinfo_wid = componentinfo_win.ComponentInfoWindow(self)
        self.update_widget(self.componentinfo_wid)
        
    def typebranch(self):
        if self.type_name == "MODULE":
            self.connectdb_wid = connectdb_win.ConnectDBWindow(self)
            self.update_widget(self.connectdb_wid)
        else:
            QMessageBox.critical(None, 'Worning',' TestType:MODULE is only supported. ', QMessageBox.Ok)

    def choose_test(self,username,password):
        self.db_user = username
        self.db_pass = password
        
        self.result_dict['user'] = self.db_user

        try:
            self.localDB = localdb_authenticator.connectDB()
            localdb_authenticator.authenticator(self.localDB,self.db_user,self.db_pass)
            self.module_info = self.localDB[self.col_dict['module_info']].find_one( { 'serialNumber':self.result_dict['component'] } )
            self.user_info = self.localDB[self.col_dict['user_info']].find_one( { 'userName':self.db_user } )
            self.institution_info = self.localDB[self.col_dict['institution_info']].find_one( { 'institution':self.result_dict['institution'] } )
            
            self.result_dict['localDB']['component'] = str( self.module_info['_id'] )
            self.result_dict['localDB']['user_id'] = str( self.user_info['_id'] )
            self.result_dict['localDB']['address'] = str( self.institution_info['_id'] )
            ######should stage info be retrieved from localDB???????
            self.result_dict['localDB']['stage'] = self.result_dict['currentStage'] #temp

            self.module_isRegistered = True
            self.choosetest_wid = choosetest_win.ChooseTestWindow(self)
            self.update_widget(self.choosetest_wid)

        except:
            print (traceback.format_exc() )
            self.module_isRegistered = False
                    
            if self.isPractice:
                self.practice_choosetest()

            else:
                msgBox = QMessageBox.warning(None, 'Worning','Authentification failed. Do you want to continue as plactice mode?', QMessageBox.Yes | QMessageBox.No )                

                if msgBox == QMessageBox.Yes:
                    self.isPractice = True
                    self.practice_choosetest()
                elif msgBox == QMessageBox.No:
                    self.isPractice = False                    
                    self.connectdb_wid = connectdb_win.ConnectDBWindow(self)
                    self.update_widget(self.connectdb_wid)

    def practice_choosetest(self):
        self.result_dict['localDB']['component'] = 'practice'
        self.result_dict['localDB']['user_id'] = 'practice'
        self.result_dict['localDB']['address'] = 'practice'
        self.result_dict['localDB']['stage'] = 'practice'
        
        self.choosetest_wid = choosetest_win.ChooseTestWindow(self)
        self.update_widget(self.choosetest_wid)

    def call_targetGUI(self):
        try:
            testGUI = os.path.join(os.path.dirname(__file__), './each_test/' + self.result_dict['localDB']['testType'] + '/' + self.result_dict['localDB']['testType'] +'.py' )
            
            self.test_module = machinery.SourceFileLoader( self.result_dict['localDB']['testType'] , testGUI).load_module()
            test_class =  str('self.test_module.' + self.result_dict['localDB']['testType'] + 'Window(self)' )
            
            self.test_win = eval( test_class )
            self.call_another_window(self.test_win)
        except:
            QMessageBox.warning(None, 'Worning','There is no library : '+testGUI, QMessageBox.Ok)
        
    def recieve_result(self,window,result_dict):
        self.latest_test = window
        self.latest_test.hide()

        result = self.trans_json_to_dict(result_dict)

        self.result_dict['results'].update(result)
        self.comfirm_result()

    def comfirm_result(self):
        print(self.result_dict)
        try:
            test_confirm = os.path.join(os.path.dirname(__file__), './each_test/' + self.result_dict['localDB']['testType'] + '/lib/confirm.py' )
            
            self.confirm_module = machinery.SourceFileLoader( 'confirm' , test_confirm).load_module()
            #            confirm_class =  str('self.confirm_module.ConfirmWindow(self)' )
        except:
            try:
                test_confirm = os.path.join(os.path.dirname(__file__), './GUI/Module_QC/lib/ConfirmWindow/Module_default.py')
                self.confirm_module = machinery.SourceFileLoader( 'Module_default' , test_confirm).load_module()
            except:
                try:
                    test_confirm = os.path.join(os.path.dirname(__file__), './GUI/Module_QC/lib/ConfirmWindow/default_layout.py')
                    self.confirm_module = machinery.SourceFileLoader( 'default_layout' , test_confirm).load_module()
                except:
                    print (traceback.format_exc() )
                    QMessageBox.warning(None, 'Worning','Module: "Module_default" or "default_layout.py" is not found', QMessageBox.Ok)
                    sys.exit(1)
                    #            self.test_win = eval( test_class )
        self.confirm_wid = self.confirm_module.ConfirmWindow(self)
        self.update_widget(self.confirm_wid)
        
    def confirm_json(self):
        try:
            json_confirm = os.path.join(os.path.dirname(__file__), './GUI/Module_QC/lib/ConfirmWindow/json_layout.py')
            self.confirm_module = machinery.SourceFileLoader( 'json_layout' , json_confirm).load_module()
            self.json_wid = self.confirm_module.JsonWindow(self)
            self.update_widget(self.json_wid)
        except:
            print (traceback.format_exc() )
            QMessageBox.warning(None, 'Worning','Module: "json_layout.py" is not found', QMessageBox.Ok)

    def back_to_test(self):
        self.back_to_last_window(self.latest_test)

##########################################
##################below is to be developed
    def upload_to_db(self):
        #        self.trans_json_to_dict(data_dict):

#        DB_address = self.DB_IPaddress[self.device_type]
#        DB_port = self.DB_port[self.device_type]
#        DB_name = self.db_dict[self.device_type]
#        DB_col =  self.col_dict[self.device_type]['testResult']

        if self.isOK_to_upload:
            try:
                db_uploader.localdb_uploader(self.localDB,self.result_dict)
#                db_uploader.general_uploader(DB_address,DB_port,DB_name,DB_col,self.result_dict)
                self.terminate_message()
            except:
                print (traceback.format_exc() )
                QMessageBox.warning(None, 'Worning','failed to upload result to DB', QMessageBox.Ok)
        else:
                QMessageBox.warning(None, 'Worning','you can not have prepared to upload', QMessageBox.Ok)            

    def terminate_message(self):
        msgBox = QMessageBox()
        CERN_icon = QPixmap(os.path.join(os.path.dirname(__file__), './GUI/Module_QC/icon/Logo-Outline-web-Blue100.png') )
        msgBox.setStyleSheet("QPushButton{font-size: 12px;}");
        msgBox.setIconPixmap(CERN_icon)
        msgBox.setStandardButtons(QMessageBox.Yes | QMessageBox.No)

        if self.isPractice == True:
            msgBox.setText('<center><font size="7">Good practice!</font></center>')
            msgBox.setInformativeText('<center><font size="5">Do you want to go actual inspection?</font></center>')
        elif self.isPractice == False:
            msgBox.setText('<center><font size="7">Thank you for your inspection!!</font></center>')
            msgBox.setInformativeText('<center><font size="5">Do you continue next inspection?</font></center>')

        button_alt = msgBox.exec()
        if button_alt == QMessageBox.Yes:
            self.isPractice = False
            self.device_type=''
            reset_result_dict()
            self.init_ui()
        elif button_alt == QMessageBox.No:
#            sys.exit()
            self.finish_GUI()


    def finish_GUI(self):
        print( self.close() )
        '''
            def write_to_localdb(self):
            dic = {
            "component_name":self.moduleid,
            "inspector":self.inspector,
            "stage":self.stage,
            "anomaly":self.anomaly_list,
            "comment":self.comment_list,
            "img":self.img_list
            }
            dic_json = json.dumps(dic)
            
            if self.module_isRegistered:
            localdb_uploader.uploader('visual_inspection',dic_json)        
            localdb_uploader.updater('visual_inspection',self.moduleid,self.stage,self.img_list)
            localdb_uploader.find_and_show('visual_inspection',self.moduleid,self.stage)
            else:
            with open('temp/result.json', 'w') as f:
            json.dump(dic_json, f, indent=4)
            
            # try:
            #     localdb_uploader.updater('visual_inspection',self.moduleid,self.stage,self.img_list)
            # except NameError:
            #     print('Image does not exist. Finish Inspection')
            
            def go_to_summary(self):
            summary_wid = summary_win.SummaryWindow(self)
            self.update_widget(summary_wid)
            
        '''


    def init_result_dict(self):
        temp_dict={
            'component':'',
            'componentType':'',
            'user':'',
            'institution':'',
            'currentLocation':'',
            'type':'',
            'currentStage':'',
            'date':'',
            'testType':'',
            'version':'',            
            'localDB':{
                'component' : '',
                'user_id' : '',
                'address' : '',
                'stage' : '',
                'testType' : '',
                'sys' : {
                    'cts' : datetime.now(),
                    'mts' : datetime.now(),
                    'rev' : 0
                },
                'testType' : '',
                'dbVersion' : 0.01
            },
            'results' : {
            }
        }
        return temp_dict

    def reset_result_dict(self):
        pass

    def all_reset_result_dict(self):
        init_result_dict()
        pass
        
    def trans_json_to_dict(self,trans_file):
        try:
            return json.load(trans_file)
        except:
            try:
                return json.loads(trans_file)
            except:
                if type(trans_file) is dict:
                    return trans_file
                else:
                    QMessageBox.warning(None, 'Worning','result should be <dict> class', QMessageBox.Ok)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    main_window = MainWindow()
    main_window.show()
    sys.exit(app.exec_())
            


